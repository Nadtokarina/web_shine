import VueRouter from "vue-router"

import MainPage from '../pages/MainPage'
import LocationPage from '../pages/LocationPage'
import ServicePage from '../pages/ServicePage'
import AboutPage from '../pages/AboutPage'
import RegistrPage from '../pages/RegistrPage'
import AuthPage from '../pages/AuthPage'
import LocationDescriptPage from '../pages/LocationDescriptPage'
import ServiceDescriptPage from '../pages/ServiceDescriptPage'



export default new VueRouter( {
    mode: 'history',
    routes: [
        {path: '/', component: MainPage},
        {path: '/location', component: LocationPage},
        {path: '/service', component: ServicePage},
        {path: '/about', component: AboutPage},
        {path: '/registr', component: RegistrPage},
        {path: '/auth', component: AuthPage},
        {path: '/location/:id', component: LocationDescriptPage, props: true, name: 'LocationDescriptPage'},
        {path: '/service/:id', component: ServiceDescriptPage, props: true, name: 'ServiceDescriptPage'},
    ]
})