import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router/router'
import vuetify from './plugins/vuetify'
import store from './store'
import VueCardCarousel from "vue-card-carousel";

Vue.use(VueRouter)
Vue.use(VueCardCarousel)


Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  vuetify,
  router,
  store
}).$mount('#app')
