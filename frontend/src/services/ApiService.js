import axios from 'axios';

class ApiService {
    sendVisitData(visitInfo) {
        return axios.post('http://127.0.0.1:8000/save_log/', visitInfo);
    }
}

export default new ApiService();