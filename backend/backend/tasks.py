from backend.celery import app
from photostudio.views import save_log


@app.task
def process_request(request):
    save_log(request)