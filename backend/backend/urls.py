"""Module providingFunction printing python version."""

from django.contrib import admin
from django.urls import path, include, re_path
from photostudio.views import CityViewSet, SubwayViewSet, LocationViewSet, ServiceViewSet, VisitorViewSet
from rest_framework import routers

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from photostudio.views import save_log

SchemaView = get_schema_view(
    openapi.Info(
        title="Your Project API",
        default_version='v1',
        description="API documentation",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

router = routers.DefaultRouter()
router.register(r'city', CityViewSet)
router.register(r'subway', SubwayViewSet)
router.register(r'location', LocationViewSet)
router.register(r'service', ServiceViewSet)
router.register(r'visitor', VisitorViewSet)

# app_name = 'oauth2_provider'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),

    path(r'swagger/', SchemaView.with_ui('swagger', cache_timeout=0)),


    path('save_log/', save_log, name='save_log'),

    path('auth/', include('djoser.urls')),
    re_path(r'^auth/', include('djoser.urls.authtoken')),
    re_path('auth/', include('rest_framework_social_oauth2.urls')),
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(
        settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
