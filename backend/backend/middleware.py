from django.utils.deprecation import MiddlewareMixin
from photostudio.views import save_log

# class LogMiddleware(MiddlewareMixin):
#     def process_request(self, request):
#         log_save(request)



class LogMiddleware(MiddlewareMixin):
    def process_request(self, request):
        save_log(request)
