from django.utils import timezone
import logging
from django.http.response import HttpResponse
from .models import City, Subway, Location, Service, Visitor, Logs
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from .serializers import (
    CitySerializer,
    SubwaySerializer,
    LocationSerializer,
    ServiceSerializer, 
    VisitorSerializer
    )


Logger = logging.getLogger('main')

class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer


class SubwayViewSet(viewsets.ModelViewSet):
    queryset = Subway.objects.all()
    serializer_class = SubwaySerializer

    @action(methods=['get'], detail=True)
    def city(self, request, pk=None):
        city = City.objects.get(pk=pk)
        return Response({'city' : city.city})


class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    # permission_classes = [permissions.IsAuthenticated]

    
    @action(methods=['get'], detail=True)
    def subway(self, request, pk=None):
        subway = Subway.objects.get(pk=pk)
        return Response({'subway_station' : subway.station_name})


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer


class VisitorViewSet(viewsets.ModelViewSet):
    queryset = Visitor.objects.all()
    serializer_class = VisitorSerializer
    


# def log_save(request):
#     return HttpResponse('<h1>Test</h1>')


def save_log(request):
    username = request.user.username
    timestamp = timezone.now()
    url = request.build_absolute_uri()
    browser = request.META.get('HTTP_USER_AGENT')

    log = Logs(username=username, timestamp=timestamp, url=url, browser=browser)

    log.save()