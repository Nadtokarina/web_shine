from django.contrib import admin
from .models import City, Subway, Location, Service, Visitor, Logs

admin.site.register(City)
admin.site.register(Logs)

@admin.register(Subway)
class SubwayAdmin(admin.ModelAdmin):
    list_display = ('city', 'station_name')
    list_filter = ('city',)

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'size', 'cost')
    list_filter = ('cost',)
    search_fields = ('name__startswith',)

@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('service_name', 'cost')
    list_filter = ('cost',)
    search_fields = ('service_name__startswith',)

@admin.register(Visitor)
class VisitorAdmin(admin.ModelAdmin):
    list_display = ('name', 'email')

