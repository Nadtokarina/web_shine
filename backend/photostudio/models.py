from django.db import models

class City(models.Model):
    city = models.CharField(verbose_name='Название города', max_length=254)

    def __str__(self):
        return self.city
    
    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Subway(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    station_name = models.CharField(verbose_name='Станция метро', max_length=255)

    def __str__(self):
        return self.station_name
    
    class Meta:
        verbose_name = 'Станция метро'
        verbose_name_plural = 'Станции метро'


class Location(models.Model):
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    subway_station = models.ForeignKey(Subway, on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Название зала', max_length=255)
    size = models.PositiveSmallIntegerField(verbose_name='Площадь зала')
    description = models.TextField(verbose_name='Описание зала')
    cost = models.PositiveSmallIntegerField(verbose_name='Цена')
    location_photo = models.ImageField(verbose_name='Фото зала', upload_to='locations', null=True)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Локация'
        verbose_name_plural = 'Локации'


class Service(models.Model):
    service_name = models.CharField(verbose_name='Название услуги', max_length=255)
    cost = models.PositiveSmallIntegerField(verbose_name='Цена услуги')
    description = models.TextField(verbose_name='Описание услуги')
    service_img = models.ImageField(verbose_name='Фото услуги', upload_to='services', null=True)

    def __str__(self):
        return self.service_name

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'


class Visitor(models.Model):
    name = models.CharField(verbose_name='Имя посетителя', max_length=255)
    email = models.EmailField(max_length=255, verbose_name='Электронный адрес посетителя')

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Посетитель'
        verbose_name_plural = 'Посетители'


class Logs(models.Model):
    username = models.CharField(verbose_name='Имя пользователя', max_length=255)
    timestamp = models.DateTimeField(verbose_name='Дата', auto_now_add=True)
    url = models.URLField(verbose_name='URL')
    browser = models.CharField(verbose_name='Браузер', max_length=255)

    def __str__(self):
        return f"{self.username} -- {self.timestamp} -- {self.browser}"
    
    class Meta:
        verbose_name = 'Логг'
        verbose_name_plural = 'Логги'